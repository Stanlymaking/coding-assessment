USE [dbCodingAssesment]
GO
/****** Object:  Table [dbo].[Tbl_ContacPerson]    Script Date: 04/12/2021 05:39:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ContacPerson](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[phone_number] [varchar](12) NOT NULL,
	[email_Address] [varchar](50) NOT NULL,
	[company] [varchar](50) NOT NULL,
	[country] [varchar](50) NOT NULL,
	[zip_code] [varchar](5) NOT NULL,
	[state] [varchar](50) NOT NULL,
	[city] [varchar](50) NOT NULL,
	[image_url] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_ContacPerson] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
